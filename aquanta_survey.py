import pandas as pd
import numpy as np
pd.set_option('max_colwidth', 400)
# Richs-MBP:pandas_examples randrews$ iconv -f ISO-8859-1 -t utf-8 < Aquanta_5-2-19_Customer_Survey_Data.csv > survey_unpivoted.csv
df = pd.read_csv("survey_unpivoted.csv", sep=",", quotechar='"', encoding = "utf-8")
#columns  = 'response', 'responder_ip', 'responder_country', 'responder_city', 'survey_created_on', 'survey_finished_on', 'survey_finished', 'question_type', 'question_name', 'responder_answer']
columns = df.columns

# replace newline things in text area response
df.replace(r'\r', u' ', regex=True, inplace=True)

# group by checkbox questions and concatenate responses

def myfunc(x):
    # a checkbox group
    #print (x['question_type'].values[0])
    if x['question_type'].values[0] == 'checkbox':
        x['responder_answer'] = ', '.join(x['responder_answer'].values)
        #print(x['responder_answer'].transform(lambda y: ','.join(y)))
    return x

df = df.groupby(['response', 'responder_ip', 'responder_country', 'responder_city', 'survey_created_on', 'survey_finished_on', 'survey_finished', 'question_type', 'question_name'], axis=0).apply(myfunc)
#print(df)

df1 = pd.pivot_table(df, index=['responder_ip', 'survey_created_on'], columns='question_name', values='responder_answer', aggfunc='last')
df1.to_csv('survey_pivoted.csv')

#df1 = df.pivot(columns=['question_name'])
#df1 = df1.sort_values(['ID']).dropna().reset_index().drop(['index'], axis=1)

