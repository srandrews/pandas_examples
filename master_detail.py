import pandas as pd
import csv
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""A,B,C,D
FRA,MAD,10HR,A
FRA,MAD,10HR,B
FRA,MAD,10HR,C
MAD,FRA,11HR,G
MAD,FRA,11HR,F
MAD,FRA,11HR,H
MAD,FRA,11HR,K""")

df = pd.read_csv(csvdata, sep=",")
df.set_index(['A', 'B', 'C'], inplace=True)
print(df)
