import pandas as pd
print(pd.__version__)

df = pd.DataFrame({'GHI':[1,0.5,0.5], 'MNO':[.5,1,.5], 'JKL':[0.5, 0.5, 1]})
print(df)
print(df.T)
#print(df.pivot(index=['GHI', 'MNO', 'JKL'], columns='', values=''))
print(df.pivot(index=['GHI', 'MNO', 'JKL'], columns=['GHI', 'MNO', 'JKL'], values=['GHI', 'MNO', 'JKL']))
