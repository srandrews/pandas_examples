import pandas as pd
from pandas.compat import StringIO
print(pd.__version__)

csvdata = StringIO("""ID,Items
0,20,A
1,20,A
2,20,B
3,20,B
4,20,B
5,20,B
6,20,A
7,21,A
8,21,B
9,21,A
10,21,B
11,21,C
12,21,C
13,21,C
14,21,C
15,21,A""")

df = pd.read_csv(csvdata)

df['streak_group'] = (df['Items'] != df['Items'].shift()).cumsum()
df = df.groupby(['ID', 'Items', 'streak_group']).size().to_frame()
df.reset_index(inplace=True)
df.columns =['ID', 'Items', 'streak_group',  'streak_size']
df['streak_kind'] = df['Items']+df['streak_size'].apply(str)
df.drop(['streak_group', 'streak_size'], axis=1, inplace=True)
df.drop_duplicates(inplace=True)
print(df)
print(df.groupby('ID')['streak_kind'].value_counts())
print(df['streak_kind'].value_counts())

