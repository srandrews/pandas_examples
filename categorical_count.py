import pandas as pd

venue = ["Fast Food", "Pizza Place", "Burger Joint", "Fast Food", "Pizza Place", "Burger Joint", "Burger Joint", "Fast Food", "Fast Food"]
df = pd.DataFrame({"Venue":venue})
df["Venue Category"] = pd.Categorical(df['Venue'])
print(df["Venue Category"].value_counts())
