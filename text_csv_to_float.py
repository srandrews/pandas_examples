import pandas as pd
import numpy as np
from pandas.compat import StringIO
print(pd.__version__)

data = """
Note|HHVA|Xpos|Ypos|B-V|e_Vmag|e_B-V
1|10001|71.20|68.87|0.731|0.010|0.010
2|10001| |68.87|0.731|0.010|0.010
"""

def myconverter(x):
    try:
        f = float(x)
    except ValueError as ve:
        return np.nan
    return f

df = pd.read_csv(StringIO(data), converters={'Xpos':myconverter}, sep='|', skiprows=0)
print(df.dtypes)
