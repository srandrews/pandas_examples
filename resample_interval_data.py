import pandas as pd
import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO

csvdata = StringIO("""startdatetime,endatetime,value
2019-03-26 23:00:00.000,2019-03-27 01:00:00.000,37.86
2019-03-27 01:00:00.000,2019-03-27 03:00:00.000,37.91
2019-03-27 03:00:00.000,2019-03-27 05:00:00.000,34.54""")

df = pd.read_csv(csvdata, sep=",", index_col="startdatetime", parse_dates=True, infer_datetime_format=True)

# flexibility to statistically pick resampled values should the index
# not be on a ten minute boundary
df = df.resample('15T').last()
df = df.reset_index()

# now that the DataFrame has a ten minute freq index, use it to make the end interval
enddatetime = df['startdatetime']
enddatetime = enddatetime.append(pd.Series(enddatetime.values[-1] +  pd.Timedelta(minutes=15)))
enddatetime = enddatetime.shift(-1).values[:-1]
df['endatetime'] = enddatetime

# flexibility to fill missing values
df['value'] = df['value'].ffill()

# results
print( df)
