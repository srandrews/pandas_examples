import pandas as pd
import numpy as np
print(pd.__version__)

df = pd.DataFrame(np.random.choice([1,np.nan,8], size=(10,1)), columns=['a'])
# TODO why does the apply fail when there are two columns
#df = pd.DataFrame(np.random.choice([1,np.nan,8], size=(10,2)), columns=['a', 'b'])

cols = df.columns

def cond_fill(s):
    fill = False
    for i,x in s.iteritems():
        if pd.isnull(x) and fill: s.loc[i] = 9
        else: fill = False

        if x == 8: fill = True

    return x

df.apply(cond_fill)

print(df)
