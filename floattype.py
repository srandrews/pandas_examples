import pandas as pd
import numpy as np
print(pd.__version__)

df = pd.DataFrame({'a': [1, 2, 3], 'b': [0., 0., 0.]})
df['a'] = df['a'].apply(lambda row: row+1)
df['b'] = df['b'].apply(lambda row: row+1)
print(df)
print(df['a'].dtype)
print(df['b'].dtype)

#df = pd.DataFrame({'a': [1, 2, 3], 'b': [0, 0, 0]})['a'].dtype
#print(df)

exit(0)

# float64 is the default dtype of an empty dataframe.
df = pd.DataFrame({'a': [], 'b': []})['a'].dtype
print(df)
try:
    df['a'] = [1,2,3,4]
except TypeError as te:
    # good, the default dtype is float64
    print(te)
print(df)

# even if 'defaul' is changed, this is a surprise 
# because referring to all columns does convert to float
df = pd.DataFrame(columns=["col1", "col2"], dtype=np.int64)
# creates an index, "a" is float type
df.loc["a", "col1":"col2"] = np.int64(0)
print(df.dtypes)

df = pd.DataFrame(columns=["col1", "col2"], dtype=np.int64)
# not upcast
df.loc[:"col1"] = np.int64(0)
print(df.dtypes)
