import pandas as pd
from pandas import Timestamp
import numpy as np
import time

s = {1: Timestamp('1970-01-28 05:28:52.235000'),
 2: Timestamp('1971-02-02 12:13:23.230000'),
 3: Timestamp('1970-09-04 17:14:53.120000')}

f = pd.DataFrame().from_dict(s, columns=["start_time"], orient='index')
f["first_time"] = f["start_time"].values[0]
f["end_time"] = f["start_time"].shift(1)

f["total_delta_time"] = f["start_time"] - f["first_time"]
f["total_delta_time_ms"] = f["total_delta_time"].astype(np.int64) // 10**9

f["row_delta_time"] = f["start_time"] - f["end_time"]
f["row_delta_time_ms"] = f["row_delta_time"].astype(np.int64) // 10**9

print(f[["total_delta_time", "total_delta_time_ms"]])

