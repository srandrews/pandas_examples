import pandas as pd
import json
from pandas.compat import StringIO
print(pd.__version__)

tz_aware = StringIO("""timestamp,tank_volume
2019-06-01T20:44:34Z,0""")

tz_naive = StringIO("""timestamp,tank_volume
2019-06-01T20:44:34,0""")

#empty = StringIO("""timestamp,tank_volume """)
s_empty = pd.Series([], index=pd.DatetimeIndex([]), name='tank_volume')

s_tz_aware = pd.read_csv(tz_aware, parse_dates=True, index_col='timestamp', dtype=str, usecols=[u'timestamp', u'tank_volume'])
s_tz_naive = pd.read_csv(tz_naive, parse_dates=True, index_col='timestamp', dtype=str, usecols=[u'timestamp', u'tank_volume'])
#s_empty = pd.read_csv(tz_naive, parse_dates=True, index_col='timestamp', dtype=str, usecols=[u'timestamp', u'tank_volume'])

pd.concat([s_tz_aware, s_tz_naive, s_empty])



exit(0)


# on pandas 0.20.3
# but it works when there are two columns and a missing one is specified
# gives ValueError: Usecols do not match names.
#s = pd.read_csv(missing_prop_and_three_cols, parse_dates=True, index_col='timestamp', dtype=str, usecols=[u'timestamp', u'TOP_TEMP'])
#print(s)

# Does squeeze cause the bug?
# yep... it assumes returns the present single column
#s = pd.read_csv(missing_prop_and_two_cols, parse_dates=True, index_col='timestamp', dtype=str, usecols=[u'timestamp', u'TOP_TEMP'], squeeze=True)
s = pd.read_csv(missing_prop_and_two_cols, parse_dates=True, index_col='timestamp', dtype=str, usecols=[u'timestamp', u'TOP_TEMP'])
s = s['TOP_TEMP']
assert(s.name == 'TOP_TEMP')
print(s)
exit(0)
# what if we don't load the index column
# gives pandas.errors.EmptyDataError: No columns to parse from file
s = pd.read_csv(missing_prop_and_two_cols, parse_dates=True, dtype=str, usecols=[u'timestamp', u'TOP_TEMP'], squeeze=True)
print(s)
s = pd.read_csv(has_prop, parse_dates=True, index_col='timestamp', dtype=str, usecols=[u'timestamp', u'TOP_TEMP'], squeeze=True)
print(s)
