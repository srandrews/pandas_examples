import pandas as pd
from pandas.compat import StringIO
import xlsxwriter
import xlwt
import openpyxl

csvdata = StringIO("""date,LASTA,LASTB,LASTC
1999-03-15,-2.5597,8.20145,16.900
1999-03-17,2.6375,8.12431,17.125
1999-03-18,2.6375,-8.27908,16.950
1999-03-19,2.6634,8.54914,17.325
1999-04-06,2.8537,7.63703,17.750""")

df = pd.read_csv(csvdata, sep=",", index_col="date", parse_dates=True, infer_datetime_format=True)

# Create a Pandas Excel writer using XlsxWriter as the engine.
writer = pd.ExcelWriter("styletest.xlsx", engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
df.to_excel(writer, sheet_name='Sheet1')

# Get the xlsxwriter workbook and worksheet objects.
workbook  = writer.book
worksheet = writer.sheets['Sheet1']

# Add some cell formats.
format1 = workbook.add_format({'num_format': '#,##0.00'})
format2 = workbook.add_format({'num_format': '0%'})
format3 = workbook.add_format({'bg_color': 'yellow'})

# Set the column width and format.
worksheet.set_column('B:B', 18, format2)
worksheet.set_row(2, 5, format3)

# Set the format but not the column width.
worksheet.set_column('C:C', None, format2)

# Close the Pandas Excel writer and output the Excel file.
writer.save()

