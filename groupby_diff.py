import pandas as pd
import numpy as np

df = pd.DataFrame(np.random.randint(1,10,size=(6,2)),columns = list("AB"))
df["A"] = ["1111","2222","1111","1111","2222","1111"]
df["B"] = ["2001-01-10","2001-01-02","2001-02-11","2001-03-14","2001-02-01","2001-04-14"]

df["B"] = pd.to_datetime(df["B"])

def myfunc(x):
    #x.sort_values(by=['B'])
    x["Trans Diff Days"] = x["B"].diff()
    return x["Trans Diff Days"]

new_series = df.groupby("A").apply(myfunc)
print(new_series.groupby("A").max())
