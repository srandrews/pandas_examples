import pandas as pd
import sqlite3
conn = sqlite3.connect('example.db')

c = conn.cursor()

# Create table
c.execute('''CREATE TABLE stocks
             (date text, trans text, symbol text, qty_real, price_real)''')

# Insert a row of data
c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")

# Larger example that inserts many records at a time
purchases = [('2006-03-28', 'BUY', 'IBM', 1000, 45.00),
             ('2006-04-05', 'BUY', 'MSFT', 1000, 72.00),
             ('2006-04-06', 'SELL', 'IBM', 500, 53.00),
            ]
c.executemany('INSERT INTO stocks VALUES (?,?,?,?,?)', purchases)


# Save (commit) the changes
conn.commit()


c = conn.cursor()
c.execute("select date, trans, symbol, qty_real, price_real from stocks")
list_o_tuples = c.fetchall()
df = pd.DataFrame(list_o_tuples)
df.columns = ["date", "trans", "symbol", "qty_real", "price_real"]
#print(df)

conn.row_factory = sqlite3.Row
c = conn.cursor()
c.execute("select date, trans, symbol, qty_real, price_real from stocks")
list_o_dict_like_rows = c.fetchall()
l = [dict(i) for i in list_o_dict_like_rows]
df = pd.DataFrame(l)
print(df)

conn.close()
