import pandas as pd
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""IP
192.168.1.10
192.168.1.11
192.168.1.12
192.168.1.13""")

df = pd.read_csv(csvdata)
df['IP'] = df['IP'].apply(lambda x: "X.X."+".".join(x.split('.')[2:4]))
print(df)

