import json
import itertools

json_str="""
{ "ABC":[
   {"AAA":2},
   {"BBB":12},
   {"CCC":12}
 ],
   "DEF":{"X":3, "Y":3,"Z":4}
}
"""

json = json.loads(json_str)
print(type(json))
print(json)

for i in itertools.combinations(json):
    print(i)

