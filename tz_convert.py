import pandas as pd
import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO

csvdata = StringIO("""timestamp,open,high,low,close,volume
2019-04-02 05:59:00,381.00,381.00,379.70,379.70,0
2019-04-02 05:58:00,380.90,380.90,380.85,380.85,5040
2019-04-02 05:57:00,380.85,380.95,380.65,380.95,9615
2019-04-02 05:56:00,380.60,381.20,380.60,381.00,13041
2019-04-02 05:55:00,379.80,380.60,379.80,380.60,19586""")

df = pd.read_csv(csvdata, sep=",", index_col="timestamp", parse_dates=True, infer_datetime_format=True)

print("index type {}".format(type(df.index)))
# is tz 'naive'
print("index tz None is naive {}".format(df.index.tz))

# results
print(df)

# so give it a locale, since input data is naive, 
# UTC must be presumed unless there is additional
# input data not specified in above example
df.index = df.index.tz_localize("Etc/UTC")
# is tz 'naive'
print("index tz None is naive {}".format(df.index.tz))

# now that the DateTimeIndex has a tz, it may
# be converted as desired
random_tz = "Asia/Kolkata"
df.index = df.index.tz_convert("Asia/Kolkata")

# results
print(df)

