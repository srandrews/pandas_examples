import pandas as pd
import numpy as np
import timeit
print(pd.__version__)

# random dataframe, provides ordered rangeindex
df = pd.DataFrame(np.random.randint(0,1000,size=(1000, 4)), columns=list('ABCD'))
# toss the ordered rangeindex and make the random 'A' the index
df.set_index(['A'], inplace=True)
# df is now a dataframe with an unordered index

def iterate(df):
    for i,r in df[::-1].iterrows():
        # process
        pass

def sort_and_apply(df):
    # apply order to the index by resetting it to a column
    # this indicates original row position by create a rangeindex.
    # (this also copies the dataframe, critically slowing down this function 
    # which is still much faster than iterate()).
    new_df = df.reset_index()

    # sort on the newly applied rangeindex and process
    new_df.sort_index(ascending=False).apply(lambda x:x)

if __name__ == '__main__':
    print("sort_and_apply ",timeit.timeit("sort_and_apply(df)", setup="from __main__ import sort_and_apply, df", number=50))
    print("iterate ", timeit.timeit("iterate(df)", setup="from __main__ import iterate, df", number=50))
