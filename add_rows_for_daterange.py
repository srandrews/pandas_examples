import pandas as pd
import csv
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""Currency,Date,Rate,UoM
Swedish krona,2016-01-05,1.0395,Hundreds
Swedish krona,2016-01-06,1.0422,Hundreds
Swedish krona,2016-01-07,1.0452,Hundreds
Swedish krona,2016-01-08,1.0450,Hundreds
Swedish krona,2016-01-11,1.0437,Hundreds
Swedish krona,2016-01-12,1.0422,Hundreds
Swedish krona,2016-01-13,1.0338,Hundreds
Swedish krona,2016-01-14,1.0347,Hundreds
Swedish krona,2016-01-15,1.0279,Hundreds
Swedish krona,2016-01-18,1.0371,Hundreds
US dollar,2019-03-15,8.5674,Units
US dollar,2019-03-18,8.5223,Units
US dollar,2019-03-19,8.5178,Units
US dollar,2019-03-20,8.5358,Units
US dollar,2019-03-21,8.4463,Units
US dollar,2019-03-22,8.5315,Units
US dollar,2019-03-25,8.5289,Units""")

df = pd.read_csv(csvdata, sep=",")
df = df.set_index(['Date'])
date_range = df.index.values
nk_df = pd.DataFrame(index=date_range, data={'Currency':'Norwegian krone', 'Rate':1, 'UoM':'Units'})
df = pd.concat([df, nk_df])
print(df.sort_index().head(10))
