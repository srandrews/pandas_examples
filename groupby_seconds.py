import pandas as pd
import numpy as np
import csv
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""index,id1,id2,timestamp,number
465,255,3644,2019-05-02 08:00:20.137000,1547856254
8736,255,3644,2019-05-02 8:00:25,1547856254
8739,255,3644,2019-05-02 9:00:10,1547856254
8740,255,3644,2019-05-02 9:00:15,1547856254
8749,255,3644,2019-05-02 9:01:10,1547856254
8750,255,3644,2019-05-02 9:00:20,1547856254
8751,255,3644,2019-05-02 9:00:21,1547856254
8752,255,3644,2019-05-02 9:00:22,1547856254
62,87,912,2019-05-02 5:00:00,2687892346
120,87,912,2019-05-02 11:00:05,2687892346
120,87,912,2019-05-02 11:00:00,2687892346
""")

# prep dataframe
df = pd.read_csv(csvdata, sep=",")
df['timestamp'] = pd.to_datetime(df['timestamp'])
df.sort_values(['id1', 'id2', 'timestamp'], inplace=True)

# get timedeltas
df['timestamp_shift'] = df.groupby(['id1', 'id2', 'number']).shift()['timestamp']
df['time_delta'] = df['timestamp'] - df['timestamp_shift']
five_seconds = np.timedelta64(5,'s')

df['bad'] = df['time_delta'] <= five_seconds

print(df)
