import pandas as pd
import numpy as np

np.random.seed(0)
corpus_df = pd.DataFrame(np.random.randn(10,2),columns=['arxiv_id', 'main_category'])
metadata = pd.DataFrame(np.random.randn(100,2),columns=['filename_parsed', 'categories'])


for i, paper in enumerate(corpus_df.itertuples(), 1):
    print(i, paper)
    print(paper.arxiv_id)
    #corpus_df.loc[i, 'main_category'] = metadata.loc[metadata['filename_parsed'] == paper.arxiv_id]['categories']
    print(metadata['filename_parsed'] == paper.arxiv_id)
    print(metadata.loc[metadata['filename_parsed'] == paper.arxiv_id])

    #corpus_df.loc[i, 'main_category'] = 10
    corpus_df.loc[i, 'main_category'] = metadata['filename_parsed'][metadata['filename_parsed'] == paper.arxiv_id]

