import pandas as pd
import numpy as np

def fft(a,n):
    b =[]
    for i in range(len(a)//2):
        b.append(a[i]+a[-i])
    return b,n

def fft_two(a,args):
    print(a)
    n = args[0]
    b = sum(a)
    return a*n+b,n    

col = ['loc', 'mass', 'time', 'signal']

loc = ['loc1'] * 10+['loc2'] * 10
mass = (['10kg']*5+['20kg']*5)*2
time = list(range(0,5))*4
ampl = list(np.random.rand(5))*4

a= [loc,mass,time,ampl]
pf = pd.DataFrame(a, index=col).T

pfi=pf.set_index(['loc','mass'])
print(pfi)
#for grp in pfi.groupby(["loc", "mass"]):
    #print("group {} {}".format(*grp[0]))
    #print("{}".format(grp[1]))
# args is in addition to the series received by fft_two()
print(pfi.groupby(["loc", "mass"])["signal"].apply(fft_two,args=(4,)))

#pfi['ampl'], pfi['freq']= fft_two(pfi['signal'],n)

