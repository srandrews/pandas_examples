import pandas as pd
print(pd.__version__)

times = ['2014-08-25', '2014-08-26', '2014-08-26', '2014-09-11', '2014-09-12', '2014-09-15', '2014-09-16']
locations = ['A', 'B', 'A', 'B', 'C','C', 'C']
df = pd.DataFrame({'date': times,'location': locations})

# multiple locations can be observed in a single day
df = df.pivot(index='date', columns='location', values='location')

# set up a datetime index
df.index = pd.to_datetime(df.index)

# normalize the days so an entire 7 day window can be rolled
df = df.resample('1d').last()

# count the number of observations in the window per location
# TODO: functional way to do this?
for col in df.columns:
    df['{}_7d_observations'.format(col)] = df[col].rolling(7).count()

print(df)
