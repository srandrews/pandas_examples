import pandas as pd
from StringIO import StringIO

data = StringIO("""Name,IP,Application,Count
Tom,100.100.100,MsWord,5
Tom,100.100.100,Excel,10
Fred,200.200.200,Python,1
Fred,200.200.200,MsWord,5""")

#df = pd.DataFrame(data, columns=['Name', 'IP', 'Application', 'Count'])
#df_new = df.groupby(['Name', 'IP', 'Application'])['Count'].apply(lambda x:x.astype(int).sum())

df = pd.read_csv(data)
new_df = df.groupby(['Name', 'IP']).sum()

# reset the two levels of columns resulting from the groupby()
new_df.reset_index(inplace=True)

df.set_index(['Name', 'IP'], inplace=True)
new_df.set_index(['Name', 'IP'], inplace=True)

#print(df)
new_df = new_df.join(df, lsuffix='_lsuffix', rsuffix='_rsuffix')


my_index = pd.MultiIndex(levels=[[],[]], labels=[[],[]], names=['Name', 'IP'])
formatted_df = pd.DataFrame(index=my_index, columns=['Application'])

for g in new_df.groupby(['Name', 'IP']):
    #print(g[0], g[1])
    series = g[1]['Count_lsuffix'].shift(-1)
    print(g[0][0])
    print(g[0][1])
    formatted_df.loc[g[0]] = series

print(formatted_df)
#for i in new_df.index:
#    print new_df.loc[i]
