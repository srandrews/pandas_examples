import pandas as pd
from pandas.compat import StringIO
print(pd.__version__)

SaobracajBeograd = StringIO("""1,vreme1,30,10,ishod1,tip1,opis1
2,vreme2,20,20,ishod2,tip2,opis2
3,vreme3,10,30,ishod3,tip3,opis3
4,vreme4,0,40,ishod4,tip4,opis4
""")

def geolocation(x):
    # x is a dataframe
    #print(x)

    #extract only coordinates and convert them to tuple
    coordinates_data = x[["N", "E"]]
    coordinates_data = coordinates_data.apply(tuple, axis=1)

    #add tuple coordinates to list
    coordinate_list = []
    for coord in coordinates_data:
        coordinate_list.append(coord)

    # Scatter points
    coordinate_lats, coordinate_lons = zip(*coordinate_list)
    print(coordinate_lats, coordinate_lons)


names = ["ID", "Vreme", "E", "N", "Ishod", "Tip", "Opis"]
df = pd.read_csv(SaobracajBeograd,  header = None, names = names)

# apply to the rows
df.apply(geolocation, axis=1)

print(df)

def do_visualization_stuff():
    #defult map location
    gmap = gmplot.GoogleMapPlotter(44.797140, 20.513460, 11)

    gmap.scatter(coordinate_lats, coordinate_lons, 'red', size = 150, marker = False)

    # Draw
    gmap.draw("my_map1.html")

