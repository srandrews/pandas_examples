import pandas as pd

def myCoolFunction(i):
    return i+1

my_list = [1,2,3]
df = pd.DataFrame(index=my_list, data=my_list, columns=['newValue']).apply(myCoolFunction)

