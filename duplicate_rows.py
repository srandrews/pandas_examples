import pandas as pd
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""DB,TITLE,ISSN,IBSN
M,a,1,NaN
M,d,1,NaN
M,c,1,NaN
N,b,1,NaN
N,a,1,NaN
N,d,1,NaN
O,c,1,NaN
O,e,1,NaN
O,a,1,NaN
O,b,1,NaN
M,a,2,2
N,a,2,NaN""")

df = pd.read_csv(csvdata)

def group_row(x):
    values = x['DB_r'].values
    #print(x.index, values)
    # it is important to realize that the grouping 
    # includes the first DB which is not a duplicate
    # so remove all subsequent occurrences
    #values = [x for x in values if x != values[0]]
    return values

df = df.merge(df, how='inner', on=['TITLE', 'ISSN', 'IBSN'], suffixes=('_l', '_r'))

# can't have NaNs in group keys
df.fillna(0,inplace=True)
df.set_index(['DB_l', 'TITLE', 'ISSN', 'IBSN'])
print(df)
df = df.groupby(['DB_l', 'TITLE', 'ISSN', 'IBSN']).apply(group_row).to_frame()

#print(df)

df.reset_index(inplace=True)
df.columns = ['DB_l', 'TITLE', 'ISSN', 'IBSN', 'Duplicates']
print(df)
