import pandas as pd
from io import StringIO

print((pd.__version__))

csvdata = StringIO("""Created,value
2019-04-02 05:23:00,3
2019-04-02 05:23:20,1
2019-04-02 05:24:00,7
2019-04-02 05:31:00,8
2019-04-02 05:49:00,
2019-04-02 05:51:00,
2019-04-02 05:52:00,5
2019-04-02 05:53:00,3
2019-04-02 05:57:00,
2019-04-02 05:58:00,0
2019-04-02 05:58:10,
2019-04-02 06:01:00,9
2019-04-02 06:07:00,5""")

df = pd.read_csv(csvdata, sep=",", index_col="Created", parse_dates=True, infer_datetime_format=True)
print(df)
#print(df.resample('5T').sum())
#print(df.resample('5T').sum(min_count=0))
#print(df.resample('5T').sum(min_count=1))

print(df.resample('5T').apply(lambda x: x.sum(min_count=0)))
print(df.resample('5T').apply(lambda x: x.sum(min_count=1)))
exit(0)
print(df.resample('5T').agg({'value':lambda x:x.sum(min_count=1)}))
#print(df.resample('5T').apply(lambda x: x.sum(min_count=1,skipna=False)))
exit(0)
print(df.resample('5T').agg({'value':sum}))
print(df.resample('5T').apply(lambda x: x.sum(skipna=False)))
