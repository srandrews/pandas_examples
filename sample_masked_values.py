import pandas as pd
import numpy as np

df = pd.DataFrame(np.random.randn(200,3),columns=['x1', 'x2', 'Class'])
mask1 = (df.x1 > -.4) & (df.x1 < .6)
mask2 = (df.x2 > -.4) & (df.x2 < .5)

# What do the masks look like in context?
df['mask1'] = mask1
df['mask2'] = mask1
print(df.head())

# apply the boolean masks so ranges in mask1 and mask2 are obtained
df1 = df[mask1 & mask2]

# sample the result
print(df1.sample(n=4))
