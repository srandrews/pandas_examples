import pandas as pd
import random as r
from pandas.compat import StringIO
print(pd.__version__)

daterange = pd.date_range('2019-01-01 11:00', '2019-01-01 13:00', periods = r.randint(10,30))
df = pd.DataFrame(index=daterange, data={'sensor_data': [r.randint(0,5) for i in range(len(daterange))]})

# the actual datapoints
actual_datapoints = df.copy()
actual_datapoints['actual'] = True

# resample e.g. visualization purposes
df = df.resample('5T').last().ffill()
# but let's not confuse these datapoints and the result of resampling with actual datapoints
df['actual'] = False
# for these false datapoints, delete the ones for which there is an actual
mask = df.index.isin(actual_datapoints.index)
df = df.drop(df[mask].index)

# combine actual datapoints with the resampled timeseries
df = pd.concat([actual_datapoints, df])
df.sort_index(inplace=True)

# a lookup dataframe, lookups by datetimeindex
data = """datetime,lookup_val
2019-01-01 11:00,100
2019-01-01 11:30,200
2019-01-01 12:00,300
"""
lookup_df = pd.read_csv(StringIO(data), index_col='datetime', parse_dates=True, infer_datetime_format=True)

# a really bad state machine. :-)
state=None
def statefunc(x):
    global state
    if x != state:
        state = x
        return 'edge'
    return state

df['state'] = df['sensor_data'].apply(statefunc)

# actual, resampled, and looked up values 
df = df.join(lookup_df)
print(df)
