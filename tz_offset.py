import pandas as pd
from pandas.compat import StringIO

csvdata = StringIO("""id,feed_id,created_at
7191,1009408,2019-03-10 01:55:35-05:00
7192,1009408,2019-03-10 01:56:35-05:00
7193,1009408,2019-03-10 01:57:36-05:00
7194,1009408,2019-03-10 01:58:38-05:00
7195,1009408,2019-03-10 01:59:38-05:00
7196,1009408,2019-03-10 03:00:39-04:00
7197,1009408,2019-03-10 03:01:40-04:00
7198,1009408,2019-03-10 03:02:41-04:00
7199,1009408,2019-03-10 03:03:42-04:00""")

df = pd.read_csv(csvdata, sep=",", index_col="id", parse_dates=True, infer_datetime_format=True)
df['created_at'] = pd.to_datetime(df['created_at'])
df['offset'] = df['created_at'].apply(lambda x: x.tzinfo)
df['naive'] = df['created_at'].apply(lambda x: x.replace(tzinfo=None))
# tzoffset._offset is a timedelta
df['naive and offset'] = df['naive'] + df['offset'].apply(lambda x: x._offset)
print(df)
