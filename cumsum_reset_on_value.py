import pandas as pd
from pandas.compat import StringIO
print(pd.__version__)

df = pd.DataFrame({'values':(49.925,49.928,49.945,49.928,49.925,49.935,49.938,49.942,49.931,49.952)})
df['a']=df.diff()

accumulator = 0.0
reset = False
def myfunc(x):
    global accumulator, reset
    if(reset):
        accumulator = 0.0
        reset = False
    accumulator += x 
    if abs(accumulator) > .009: 
        reset = True
    return accumulator

df['a'].fillna(value=0, inplace=True)
df['b'] = df['a'].apply(myfunc)
print(df)
