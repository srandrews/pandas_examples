import pandas as pd
from pandas.compat import StringIO

print(pd.__version__)

csvdata = """ID,week,sale
A,1,1
A,2,4
A,3,10
B,1,7
B,2,2.3
B,3,4.4"""

with open('txt.csv', 'w') as the_file:
    the_file.write(csvdata)

reader=pd.read_csv('txt.csv', chunksize=2)

sl=[]
for chunk in reader:
    # each chunk is a DataFrame
    print("Chunk type {}".format(type(chunk)))
    print("Chunk values {}".format(chunk.values))
    for row in chunk.values:
        sl.append(row)
print(sl)

df = pd.DataFrame(sl)
print(df)


