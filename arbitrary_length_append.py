import pandas as pd
import numpy as np

data_default = [0.0, False]

def append(data_frame, *args):
    dtypes = data_frame.dtypes.tolist()
    timestamps_step = data_frame.index.freq
    timestamp = data_frame.index.max() + timestamps_step
    if not args:
        args = data_default
    data = dict(zip(data_frame.columns, args))
    series = pd.Series(data, name=timestamp)
    data_frame = data_frame.append(series)
    data_frame.index.freq = timestamps_step
    for col in data_frame.columns:
        dtype = dtypes.pop(0)
        data_frame[col] = data_frame[col].astype(dtype)
    return data_frame


timestamps = pd.date_range(start='2019-04-22', end='2019-04-23')
quantities = np.array([1.0, 0.0])
is_closed = np.array([False, True])
data = dict(quantities=quantities, is_closed=is_closed)
data_frame = pd.DataFrame(data=data, columns=data.keys(), index=timestamps)

print(data_frame)

data_frame = append(data_frame, 6.0, False)

print(data_frame)

data_frame = append(data_frame, 4.0)

print(data_frame)

data_frame = append(data_frame)

print(data_frame)
