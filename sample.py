import pandas as pd

population = pd.DataFrame([[1,True],[1,False],[1,False],[2,True],[2,True],[2,False],[2, True]], columns = ['Group ID','Response'])

sample_info = pd.DataFrame([[1,2],[2,2]], columns = ['Group ID','Sample Size'])

mapper = sample_info.set_index('Group ID')['Sample Size'].to_dict()

result =  population.groupby('Group ID').apply(lambda x: x.sample(n=mapper.get(x.name))).reset_index(drop=True)

print(result)
