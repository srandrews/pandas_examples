import pandas as pd
import csv
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""project,effort,type
p1,6,feature
p2,4.5,feature
p3,4.375,bug
p4,4,bug
p5,3.875,bug
p6,3.5,upgrade
p7,3.5,feature
p8,3,upgrade
p9,2,upgrade""")

df = pd.read_csv(csvdata, sep=",")

print(df.sort_values(['effort'], ascending=False))
print(df.sort_values(['effort'], ascending=False).groupby('type').mean())
