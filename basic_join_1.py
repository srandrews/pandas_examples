import pandas as pd
import numpy as np
print(pd.__version__)

df1_string = """-25.66026   28.0914     HappyPlace
-25.67923   28.10525    SadPlace
-30.68456   19.21694    AveragePlace
-30.12345   22.34256    CoolPlace
-15.12546   17.12365    BadPlace"""

df2_string = """-25.66026   28.0914     5000
-25.14568   28.10525    1750
-30.68456   19.21694    6000
-30.65375   22.34256    8000
-15.90458   17.12365    5600"""

df1 = pd.DataFrame([x.split() for x in df1_string.split('\n')], columns=['Latitude', 'Longitude', 'Population'])
df2 = pd.DataFrame([x.split() for x in df2_string.split('\n')], columns=['Latitude', 'Longitude', 'Population'])
result = pd.merge(df1, df2, on=['Latitude'], how='inner')
print(set(df1['Latitude']).intersection(set(df2['Latitude'])))
print(df1[(df1['Latitude'] == df2['Latitude'])])
print(df1.where(df1.Latitude == df2.Latitude))

print(result)
