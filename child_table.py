import pandas as pd
import numpy as np
print(pd.__version__)

s1 = pd.Series([1, 0, 'A'])
s2 = pd.Series([2, 'df1', 'B'])
s3 = pd.Series([3, 0, 'C'])
df0 = pd.DataFrame([list(s1), list(s2), list(s3)],  columns =  ["Code1", "Code2", "Message"])

s4 = pd.Series([0, 'B1'])
s5 = pd.Series([1, 'B2'])
s6 = pd.Series([2, 'B3'])
df1 = pd.DataFrame([list(s4), list(s5), list(s6)],  columns =  ["Code2", "Message"])
join_key = 'df1'
df1[join_key] = join_key

df2 = pd.concat([df0[df0['Code2'] == join_key], df1], sort=True)

result = df0.merge(df2, left_on=['Code2'], right_on=['df1'], how='outer', suffixes=('_l', '_r'))
print (result)
