import pandas as pd
import numpy as np

np.random.seed(0)
pd1 = pd.DataFrame(np.random.randn(100,6),columns=list('ABCDEF'))
pd2 = pd.DataFrame(np.random.randn(100,6),columns=list('ABCDEF'))

pd2['maxColumn'] = pd1.max().sort_values()[-1:].index.values[0]
pd2['maxValue'] = pd1.max().sort_values()[-1:][0]

#print(pd2[['maxColumn', 'maxValue']].head(10))


np.random.seed(4)
pd1 = pd.DataFrame(np.random.randn(3,3),columns=list('ABC'))
pd2 = pd.DataFrame(np.random.randn(3,3),columns=list('ABC'))
pd2['maxRankCol1'] = pd1.idxmax(axis=1)
pd2['maxRankVal1'] = pd1.max(axis=1)
temp = pd2[pd2['maxRankCol1']]
#pd2['maxRankVal2'] = pd2[pd2['maxRankCol1']] # This statement does not work
pd2.insert(3,'maxRankVal2',0) # add Expected Results Column
print(pd2.head(4))

