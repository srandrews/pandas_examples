import pandas as pd
import sys
if sys.version_info[0] < 3:
    from StringIO import StringIO
else:
    from io import StringIO

data = StringIO("""Customer ID,SKU,Month,Value
C00005,550000,4,16
C00005,560000,5,17
C00005,560000,5,13
C00005,570000,6,18
C00005,570000,6,16
C00005,570000,6,15
C00005,560000,4,18
C00004,570000,5,17
C00004,570000,5,14
C00004,560000,6,16""")

df = pd.read_csv(data, sep=",", index_col=["Customer ID", "SKU", "Month"])

# Get count of month rows for each group
grouped_month_count = df.groupby(['Customer ID', 'SKU', 'Month']).count()
grouped_month_count.columns = ['Month Count']

# get sum of values for each group
summed_values = df.groupby(['Customer ID', 'SKU', 'Month']).sum()
summed_values.columns = ['Value Sum']

result = grouped_month_count.join(summed_values)
result['Average'] = result['Value Sum']/result['Month Count']
print(result)
