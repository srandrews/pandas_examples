import pandas as pd
import numpy as np

arrays = [['bar', 'bar', 'baz', 'baz', 'foo', 'foo', 'qux', 'qux'], ['one', 'two', 'one', 'two', 'one', 'two', 'one', 'two']]
tuples = list(zip(*arrays))
index = pd.MultiIndex.from_tuples(tuples, names=['first', 'second'])
df1 = pd.DataFrame(np.random.randn(8,2), index=index, columns=['ColA', 'ColB'])
print(df1)

arrays = [['bar', 'bar', 'foo', 'foo', 'mux', 'mux'], ['one', 'two', 'one', 'two', 'one', 'two']]
tuples = list(zip(*arrays))
index = pd.MultiIndex.from_tuples(tuples, names=['first', 'second'])
df2 = pd.DataFrame(np.random.randn(6,2), index=index, columns=['ColC', 'ColD'])
print(df2)

# a left join on df1 will show what didn't match in df2
result = df1.join(df2, how='left')
#print(result[~(result['ColC'].isnull() | result['ColD'].isnull())])

# a typical inner join will provide the intersaction of the two
result = df1.join(df2, how='inner')
print(result)
