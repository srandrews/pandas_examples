import pandas as pd

# period index strategy
df = pd.DataFrame({"y": [1,2,3,4,5]}, pd.to_datetime(["2000-03-31 00:00:00", "2000-05-31 00:00:00", "2000-08-31 00:00:00", "2000-11-30 00:00:00", "2001-01-31 00:00:00"]))
df.index = df.index.to_period("D")
print(df)

# timedelta strategy
df = pd.DataFrame({"y": [1,2,3,4,5]}, pd.to_datetime(["2000-03-31 00:00:00", "2000-05-31 00:00:00", "2000-08-31 00:00:00", "2000-11-30 00:00:00", "2001-01-31 00:00:00"]))
timedeltas = df.index.to_series().diff()
df['timedeltas'] = timedeltas
df['days'] = df['timedeltas'].apply(lambda x:x.days)
df['hours'] = df['timedeltas'].apply(lambda x:x.seconds/3600)
df.set_index(['days', 'hours'], inplace=True)
print(df)

# string strategy
df = pd.DataFrame({"y": [1,2,3,4,5]}, pd.to_datetime(["2000-03-31 00:00:00", "2000-05-31 00:00:00", "2000-08-31 00:00:00", "2000-11-30 00:00:00", "2001-01-31 00:00:00"]))
df['month'] = df.index.month
df['day'] = df.index.day
df.set_index(['month', 'day'], inplace=True)
print(df)

# time grouping
df = pd.DataFrame({"y": [1,2,3,4,5]}, pd.to_datetime(["2000-03-31 00:00:00", "2000-05-31 00:00:00", "2000-08-31 00:00:00", "2000-11-30 00:00:00", "2001-01-31 00:00:00"]))
data = df.groupby(pd.Grouper(freq='M')).count()
print (data)
