import pandas as pd
from pandas.compat import StringIO
print(pd.__version__)

from datetime import datetime as dt

from os import listdir
from os.path import isfile, join

mypath='/tmp/csv'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

import glob, os
os.chdir(mypath)
data = []
for file in glob.glob("*.csv"):
    label = dt.strptime(file[:10], "%Y-%m-%d")
    df = pd.read_csv(file, sep=",")
    data.append( (label, df['online_ratio'].mean()))

df = pd.DataFrame(data, columns =['index', 'online_ratio_mean'])
df = df.set_index('index')
df.sort_index(inplace=True)
print(df)
df.to_csv("out.csv")
