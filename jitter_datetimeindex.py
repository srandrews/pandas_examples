import pandas as pd
import random as r
from datetime import datetime as dt
from datetime import timedelta as td
print(pd.__version__)
pd.options.display.max_rows = 999

import random as r
from datetime import datetime as dt
from datetime import timedelta as td
start = dt(year=2020, month=1, day=1)
end = dt(year=2020, month=1, day=2)
interval_seconds = int((end - start).total_seconds())

def jittered_date(start, interval_seconds):
    # five minute steps
    step_s = 5 * 60
    # regular range of seconds
    for d in range(0, interval_seconds, step_s):
        # random number on gaussian distribution, mean 0 std 75, to get jitter
        g = r.gauss(0, 75)
        # keep jitter value from moving d behind previous or ahead of next d
        d += g if abs(g) < (step_s/2) else (step_s/2)
        yield start + td(seconds=d)

datetime_index = [x for x in jittered_date(start, interval_seconds)]
df = pd.DataFrame(index=datetime_index, data={'data': [r.randint(0,5) for i in range(len(datetime_index))]})
df = df.reset_index() 
df['diff'] = df['index'] - (df['index'].shift())
print(df)

