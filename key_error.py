

import pandas as pd
from pandas.compat import StringIO

csvdata = StringIO("""date,OVER_price,DI1J95_price
1995-01-02,48.61,45.662
1995-01-03,50.12,45.542
2019-03-11,6.40,""")

df = pd.read_csv(csvdata, sep=",", index_col="date", parse_dates=True, infer_datetime_format=True)
print(type(df.index))

from datetime import datetime as dt
print(df.at[dt(year=1995, month=1, day=3), 'OVER_price'])

csvdata.seek(0)
df = pd.read_csv(csvdata, sep=",", index_col="date", parse_dates=True, infer_datetime_format=True)
try:
    print(df.at['1995-01-03', 'OVER_price'])
except KeyError as ke:
    print("{} {}".format(type(ke), ke))

csvdata.seek(0)
df = pd.read_csv(csvdata, sep=",", index_col="date")
try:
    print(df.at[dt(year=1995, month=1, day=3), 'OVER_price'])
except KeyError as ke:
    print("{} {}".format(type(ke), ke))

