import pandas as pd
import itertools
import pprint
from random import randint
pp = pprint.PrettyPrinter(indent=4)

Never finished this... write a function that can walk mixed list of lists and dicts

# modified from https://codereview.stackexchange.com/questions/21033/flatten-dictionary-in-python-functional-style
def flatten(d):
    def items():
        if type(d) == str:
            print("Is a str")
            yield "somekey{}".format(randint(1,10)), d
        else:
            for key, value in d.items():
                if isinstance(value, dict):
                    for subkey, subvalue in flatten(value).items():
                        yield key + "." + subkey, subvalue
                elif isinstance(value, list):
                    for i in value:
                        #print("Will flatten {}".format(i))
                        flatten(i)
                else:
                    yield key, value

    return dict(items())

d = [{'address_components': [{'long_name': 'Aberdeen',
'short_name': 'Aberdeen',
'types': ['locality', 'political']},
   {'long_name': 'Aberdeen City',
'short_name': 'Aberdeen City',
'types': ['administrative_area_level_2', 'political']},
   {'long_name': 'Scotland',
'short_name': 'Scotland',
'types': ['administrative_area_level_1', 'political']},
    {'long_name': 'United Kingdom',
'short_name': 'GB',
'types': ['country', 'political']}],
 'formatted_address': 'Aberdeen, UK',
  'geometry': {'bounds': {'northeast': {'lat': 57.19565069999999,
 'lng': -2.0441627},
'southwest': {'lat': 57.1041518, 'lng': -2.2058926}},
'location': {'lat': 57.149717, 'lng': -2.094278},
'location_type': 'APPROXIMATE',
'viewport': {'northeast': {'lat': 57.19565069999999, 'lng': 
-2.0441627},
'southwest': {'lat': 57.1041518, 'lng': -2.2058926}}},
  'place_id': 'ChIJSXXXH0wFhEgRcsT0XNoFu-g',
  'types': ['locality', 'political']}]

#pp.pprint(d)

data = list(map(flatten, d))

pp.pprint(data)
exit(0)
df = pd.DataFrame(data)
print(df)
