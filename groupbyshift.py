import pandas as pd
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""ID,week,sale
A,1,1
A,2,4
A,3,10
B,1,7
B,2,2.3
B,3,4.4""")

df = pd.read_csv(csvdata, sep=",")
df['sale-shift'] = df.groupby('ID')['sale'].shift(-1)
print(df)
