# Python3 program to find highest  
# power of 2 smaller than or  
# equal to n. 
import math 

def highestPowerof2(n): 

    p = int(math.log(n, 2)); 
    return int(pow(2, p));  


lst = [20, 40, 110]

lst_power2 = [highestPowerof2(lst[i]) for i in range(len(lst))]
print(lst_power2)
lst_power2 = [highestPowerof2(i) for i in lst]
print(lst_power2)
