import pandas as pd
import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO

csvdata,=,StringIO("""date,LASTA,LASTB,LASTC,
1999-03-15,2.5597,8.20145,16.900,
1999-03-16,2.6349,8.03439,17.150,
1999-03-17,2.6375,8.12431,17.125,
1999-03-18,2.6375,8.27908,16.950,
1999-03-19,2.6634,8.54914,17.325,
1999-03-22,2.6721,8.32183,17.195,
1999-03-23,2.6998,8.21218,16.725,
1999-03-24,2.6773,8.15284,16.350,
1999-03-25,2.6807,8.08378,17.030,
1999-03-26,2.7802,8.14038,16.725,
1999-03-29,2.8139,8.07832,16.800,
1999-03-30,2.8105,8.10124,16.775,
1999-03-31,2.7724,7.73057,16.955,
1999-04-01,2.8321,7.63714,17.500,
1999-04-06,2.8537,7.63703,17.750""")

df = pd.read_csv(csvdata, sep=",", index_col="date", parse_dates=True, infer_datetime_format=True)

# flexibility to statistically pick resampled values should the index
# not be on a ten minute boundary
df = df.resample('15T').last()
df = df.reset_index()

# now that the DataFrame has a ten minute freq index, use it to make the end interval
enddatetime = df['startdatetime']
enddatetime = enddatetime.append(pd.Series(enddatetime.values[-1] +  pd.Timedelta(minutes=15)))
enddatetime = enddatetime.shift(-1).values[:-1]
df['endatetime'] = enddatetime

# flexibility to fill missing values
df['value'] = df['value'].ffill()

# results
print( df)
