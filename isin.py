import pandas as pd
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""
day,sev,era,lco,lum,ns
1,a,b,c,d,e
2,f,g,h,i,j
""")

df = pd.read_csv(csvdata, sep=",")
days = [1,3,4]
df[df.day.isin(days)]
