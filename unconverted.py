import pandas as pd
import numpy  as np
from datetime import datetime

# df1 and df2:
id_sales   = [1, 2, 3, 4, 5, 6]
col_names  = ['Id', 'parrotId', 'Dates']
df1        = pd.DataFrame(columns = col_names)
df1.Id     = id_sales
df1.parrotId = [1, 2, 3, 1, 2, 3]
df1.Dates  = ['2012-12-25', '2012-08-20', '2013-07-23', '2014-01-14', '2016-02-21', '2015-10-31']

col_names2 = ['parrotId', 'months']
df2        = pd.DataFrame(columns = col_names2)
df2.parrotId = [1, 2, 3]
df2.months = [12, ('Febt,Mar,Mar'), 0]

df3 = pd.merge(df1, df2, on = 'parrotId')
df3.Dates = pd.to_datetime(df3['Dates'], format = "%Y-%m-%d")
# determine if df3['Dates'].month is zero or one offset (is one)
#print(df3['Dates'].apply(lambda x: x.month))

#exit(0)

def matched(row):
    #print("Will process row", row)
    if type(row['months'])==str:
        # for the case ('Feb, Mar, Apr') - get numerical representation of month from your string and return True if the 'Dates' value matches with some list item
        print (row['Dates'].month)
        # determine if datetime.strptime is zero or one offset (is one)
        # print ([datetime.strptime(mon.strip()[:3], '%b').month for mon in row['months'].split(',')])
        return row['Dates'].month in [datetime.strptime(mon.strip()[:3], '%b').month for mon in row['months'].split(',')]  
    else:
        # for numbers - return True if months match
        return row['Dates'].month==row['months']

df3['DateMonth'] = df3.apply(matched, axis=1).astype(int)

datetime.strptime('Mar'[:4], '%b').month

print (df3)
