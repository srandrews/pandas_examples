import pandas as pd
from pandas.compat import StringIO
import fileinput
import glob


csvdata = str("""Export Type:                        by LAI\GCI\SAI
LAI\GCI\SAI:                        fjdfkj
HLR NUMBER:                         NA
Routing Category:                   NA
Telephone Service:                  NA
Export User Scope:                  Attached & Detached User
Task Name:                          lfl;sfd
Data Type:                          col1/col2
Begin Time of Exporting data:       2019-4-14 19:41
=================================
col1                    col2
401885464645645         54634565754
401884645645564         54545454564
401087465836453         54545454565
401885645656567         53434343435
401084569498484         54342340788
401088465836453         56767686334
401439569345656         64545467558
401012993933334         55645342352
401034545566463         34353463464""")

files = ["file{}.txt".format(i) for i in range(3)]
for fn in files:
    with open(fn, "w") as f:
        f.write(csvdata)

file_list = glob.glob("file*.txt")

dfs = []
for f in file_list:
    df = pd.read_csv(f, sep="\s+", header=[10])
    dfs.append(df)

df = pd.concat(dfs)
df.reset_index(inplace=True)

df.to_csv("resultfile.txt")

