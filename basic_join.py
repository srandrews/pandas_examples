import pandas as pd
import numpy as np


df1_string = """City_1      100      1     0
City_1      100      2     6
City_1      100      2     2
City_1      100      3     9
City_1      200      1     6
City_1      200      2     6
City_1      200      3     7
City_1      300      1     0"""

df2_string = """City_1      100      1     0
City_1      100      2     6
City_1      100      2     2
City_1      100      8     9
City_1      200      1     6
City_1      200      2     6
City_1      800      3     7
City_1      800      8     0"""

df1 = pd.DataFrame([x.split() for x in df1_string.split('\n')], columns=['City', 'Pop', 'Homes', 'Other'])
df2 = pd.DataFrame([x.split() for x in df2_string.split('\n')], columns=['City', 'Pop', 'Homes', 'Other'])

# Dataframes benefit from having indexes that reflect that tabular data
df1.set_index(['City', 'Pop', 'Homes'], inplace=True)
df2.set_index(['City', 'Pop', 'Homes'], inplace=True)

# an inner join on the multiindex will provide the intersaction of the two
result = df1.join(df2, how='inner', on=['City', 'Pop', 'Homes'], lsuffix='_l', rsuffix='_r')

# a join provides all of the joined columns
result.reset_index(inplace=True)
result.drop(['Other_r'], axis=1, inplace=True)
result.columns = ['City', 'Pop', 'Homes', 'Other']

print(result)
