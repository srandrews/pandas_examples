import pandas as pd
from pandas.compat import StringIO
print(pd.__version__)

data =  """Country    Value1    Value2
US         1         3
UK         3         2
US         2         1
UK         5         5"""

df = pd.read_csv(StringIO(data), sep='\s+')
df = df.groupby('Country').apply(lambda x: x.where(x > 1).count())
print(df)
