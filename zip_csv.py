import pandas as pd
import zipfile as zf
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""index,id1,id2,timestamp,number
465,255,3644,2019-05-02 08:00:20.137000,123123
62,87,912,2019-05-02 5:00:00,435456
""")

# prep dataframe
df = pd.read_csv(csvdata, sep=",")
df.to_csv('test1.csv', compression='zip')
df.to_csv('test2.csv.zip', compression='zip')

with zf.ZipFile('archive.zip', 'w') as myziparchive:
    myziparchive.writestr('df.csv', df.to_csv())
