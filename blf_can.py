import pandas as pd
import can

print(pd.__version__)

log = [can.Message(timestamp=1551734911.0096931, arbitration_id=0x14ff0065, extended_id=True, channel=2, dlc=8, data=[0xf4, 0x7c, 0x89, 0x35, 0x28, 0xf, 0xea, 0xe]), can.Message(timestamp=1551734911.0102572, arbitration_id=0x14ff0165, extended_id=True, channel=2, dlc=8, data=[0x40, 0x14, 0x0, 0x36, 0xd0, 0x39, 0x60, 0x22]), can.Message(timestamp=1551734911.0108252, arbitration_id=0x14ff0265, extended_id=True, channel=2, dlc=8, data=[0x80, 0x35, 0x9, 0xf, 0x8c, 0x0, 0x0, 0x0]), can.Message(timestamp=1551734911.0114133, arbitration_id=0x14fef100, extended_id=True, channel=2, dlc=8, data=[0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff])]

df_data = [{'timestamp':m.timestamp, 'arbitration_id':m.arbitration_id, 'extended_id':m.is_extended_id, 'channel':m.channel, 'dlc': m.dlc, 'data':m.data} for m in log]
df = pd.DataFrame(df_data)
