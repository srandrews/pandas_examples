import pandas as pd
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""date,LASTA,LASTB,LASTC
1999-03-15,2.5597,8.20145,16.900
1999-03-31,delayed,7.73057,16.955
1999-04-01,2.8321,7.63714,17.500
1999-04-06,2.8537,delayed,delayed""")

df = pd.read_csv(csvdata)

# debugging complex lambdas is sometimes better done by
# passing in a function to see what is going on
def row(x):
    print(type(x))
    match = x.str.contains('delayed').any()
    return match 

df['function_match'] = df.apply(row, axis=1)
df['lambda_match'] = df.apply(lambda row: row.str.contains('delayed').any(), axis=1)

# use the match column as a boolean mask, and then index by preferred column
print(df[df['lambda_match']]['LASTA'])
