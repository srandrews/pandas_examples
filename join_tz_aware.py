import pandas as pd
from pandas.compat import StringIO

print(pd.__version__)

s1 = pd.Series([1], index=pd.DatetimeIndex(['2019-06-08 01:22:01Z']), name='foo')
s2 = pd.Series([1], index=pd.DatetimeIndex(['2019-06-08 01:22:01']), name='foo')
#s2 = pd.Series([], index=pd.DatetimeIndex([]), name='bar')

df = pd.concat([s1, s2])
print(df)
