# -*- coding: utf-8 -*-
import pandas as pd
from io import StringIO
import sys

print(sys.version)
print(pd.__version__)

csv = u'name\n"BOVÉR, LIÙSAIDH"'
print(type(csv))
df = pd.read_csv(StringIO(csv), index_col=False, encoding='utf-8')
print(df.to_html())
