import pandas as pd
from io import StringIO

print((pd.__version__))

csvdata = StringIO("""Created,value
2019-04-02 05:23:00,1
2019-04-02 05:23:20,0
2019-04-02 05:24:00,0
2019-04-02 05:31:00,1
2019-04-02 05:49:00,
2019-04-02 05:51:00,
2019-04-02 05:52:00,1
2019-04-02 05:53:00,1
2019-04-02 05:57:00,
2019-04-02 05:58:00,0
2019-04-02 05:58:10,
2019-04-02 06:01:00,1
2019-04-02 06:07:00,""")

df = pd.read_csv(csvdata, sep=",", index_col="Created", parse_dates=True, infer_datetime_format=True)
print(df)
idx = df['value'].where(df['value'] == 1).last_valid_index()
print(df[idx:][1:].index[0])
