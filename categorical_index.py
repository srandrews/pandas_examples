import pandas as pd

colors = ["Green", "Red", "Yellow", "Yellow", "Red", "Green"]
df = pd.DataFrame({"Color":colors})
sort_dict = {"Yellow":-1, "Green":1, "Red":6}
df["colorcat"] = pd.Categorical(df['Color'], categories=sorted(sort_dict, key=sort_dict.get), ordered=True)
print(df.sort_values("colorcat"))


df["colorcat"] = pd.Categorical(df['Color'], categories=[("Green", "Yellow"), "Red"], ordered=True)
print(df.sort_values("colorcat"))

