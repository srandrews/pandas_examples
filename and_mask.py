import pandas as pd
import csv

from pandas.compat import StringIO

csvdata = StringIO("""
Income,Debt Ratio
5000000,.4
1000000,.4
1500000,.3
1500000,.1
""")

loans_df = pd.read_csv(csvdata, sep=",")
#mask = (loans_df['Income'] > 1000000) & (loans_df['Income'] < 2000000) & (loans_df['Debt Ratio'] > .20)
mask = (loans_df['Income'] == range(1000000,2000000))  & (loans_df['Debt Ratio'] > .20)
print (loans_df[mask])
