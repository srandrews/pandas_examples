import requests

from bs4 import BeautifulSoup

import numpy as np

import pandas as pd


# Lists to store the scraped data in

addresses = []
geographies = []
rents = []
units = []
availabilities = []


# Scraping all pages

pages_url = requests.get('https://www.rent.com/new-york/tuckahoe-apartments')

pages_soup = BeautifulSoup(pages_url.text, 'html.parser')

list_nums = pages_soup.find('div', class_='_1y05u').text

print(list_nums)

pages = [str(i) for i in range(0,6)]

for page in pages:

    response = requests.get('https://www.rent.com/new-york/tuckahoe-apartments?page=' + page).text

    html_soup = BeautifulSoup(response, 'html.parser')


    # Extract data from individual listing containers

    listing_containers = html_soup.find_all('div', class_='_3PdAH')
    print(type(listing_containers))
    print(len(listing_containers))
    print("Page " + str(page))



    for container in listing_containers:

        address = container.a
        if address is not None:
            addresses.append(address.text)
        elif address is None:
            addresses.append('None')
        else:
            address.append(np.nan)

        geography = container.find('div', class_='_1dhrl')
        if geography is not None:
            geographies.append(geography.text)
        elif geography is None:
            geographies.append('None')
        else:
            geographies.append(np.nan)

        rent = container.find('div', class_='_3e12V')
        if rent is None:
            rents.append('None')
        elif rent is not None:
            rents.append(rent.text)
        else:
            rents.append(np.nan)

        unit = container.find('div', class_='_2tApa')
        if unit is None:
            rents.append('None')
        elif rent is not None:
            units.append(unit.text)
        else:
            rents.append(np.nan)

        availability = container.find('div', class_='_2P6xE')
        if availability is None:
            availabilities.append('None')
        elif availability is not None:
            availabilities.append(availability.text)
        else:
            availabilities.append(np.nan)


    print(len(addresses))
    print(len(geographies))
    print(len(rents))
    print(len(units))
    print(len(availabilities))

    minlen = min(len(addresses), len(geographies), len(rents), len(units), len(availabilities))
    print('Minimum Array Length on this Page = ' + str(minlen))


    d = {'Street' : addresses,
        'City-State-Zip' : geographies,
        'Rent' : rents,
        'BR/BA' : units,
        'Units Available' : availabilities
    }
    test_df = pd.DataFrame(dict([(k,pd.Series(v)) for k,v in d.items()]))

    print(test_df)

