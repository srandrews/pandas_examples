import pandas as pd
import json
from pandas.compat import StringIO
print(pd.__version__)

csvdata = StringIO("""index,url
1,https://covers.openlibrary.org/w/id/7984916-M.jpg
""")

csv_df = pd.read_csv(csvdata, sep = ",", header = 0, index_col = False)
print(csv_df.head())

jsondata = StringIO()
csv_df.to_json(jsondata, orient = "records", date_format = "epoch", double_precision = 10, force_ascii = True, date_unit = "ms", default_handler = None)

json_str = jsondata.getvalue()
print(json_str)
a_dict = json.loads(json_str)
print(a_dict)

