import pandas as pd
import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO

csvdata = StringIO("""
customer,date,x,y,z
1,10/7/2015 0:00,4,4,
1,10/7/2015 1:00,5,9,1
1,10/9/2015 0:00,4,0,3
2,10/7/2015 0:00,8,8,4
2,10/7/2015 1:00,4,5,
3,10/7/2015 0:00,1,,
3,10/7/2015 1:00,4,0,
3,10/9/2015 0:00,4,0,
""")


df = pd.read_csv(csvdata, sep=",", index_col="date", parse_dates=True, infer_datetime_format=True)

# results
print(df)
