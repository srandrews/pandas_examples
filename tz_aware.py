import pandas as pd
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""Created,open,high,low,close,volume
2019-04-02 05:59:00,381.00,381.00,379.70,379.70,0
2019-04-02 05:58:00,380.90,380.90,380.85,380.85,5040
,380.85,380.95,380.65,380.95,9615
2019-04-02 05:56:00,380.60,381.20,380.60,381.00,13041
2019-04-02 05:55:00,379.80,380.60,379.80,380.60,19586""")

#df = pd.read_csv(csvdata, sep=",", index_col="Created", parse_dates=True, infer_datetime_format=True)
df = pd.read_csv(csvdata, sep=",")

df["Created"] = pd.to_datetime(df["Created"])
print(df)

df['Created'] = df['Created'].dt.tz_localize('Europe/London', nonexistent='shift_forward').dt.tz_convert('Europe/Paris')
print(df)
