import pandas as pd
import numpy as np

df = pd.DataFrame({'ID': ['1','1','2'], \
                   'diag1': ['C1.10', 'E10.40','F20.00'], \
                   'diag2': ['M30', '','O92.15'], \
                   'diag3': ['E15.34', 'H20.00','']})

df.replace(r'^\s*$', np.nan, regex=True, inplace=True)
df1 = pd.melt(df, id_vars=['ID'], value_vars=['diag1', 'diag2', 'diag3'], value_name='diag_all')[['ID', 'diag_all']]
df1 = df1.sort_values(['ID']).dropna().reset_index().drop(['index'], axis=1)
print(df1)

