import pandas as pd

# data example (real df is shape(188225, 2)
hu = pd.DataFrame({'Id': ['1','12','123','1234','12345'], 'Serial':['A','AB','ABC','ABC','ABC']}, dtype = 'category')
max_len = df.groupby(Id)
max_len = hu.groupby('Id')[Serial].size().max() # Find the max length 
grouped = df.groupby(Id) 

from io import StringIO
from csv import writer

output = StringIO()
csv_writer = writer(output)

for key, vals in grouped.groups.items():
    # Vector of serials with 0 padding matching so max_len = | [a, b, c, 0, 0, 0...]|
    csv_writer.writerow(np.append(np.append(key, vals.values), np.array([0] * (max_len - len(vals)))))

    output.seek(0) #goes to the start of the IO file
    dfdiscrete = pd.read_csv(output,
                             header=None,
                             index_col=0,
                             dtype=str)

print("\Discrete Serials:", len(grouped.groups), "nunique ids", hu[Id].nunique())

