import pandas as pd
from pandas.compat import StringIO
print(pd.__version__)

csvdata = StringIO("""Category: All categories
Time,enj coin: (United States)
2019-04-10T19,7
2019-04-10T20,20""")

df = pd.read_csv(csvdata, header=[0,1])
print(df)
