import pandas as pd
import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO

train_data = StringIO("""node1_id node2_id is_chat
8446602 6636127 0
1430102 7433949 0
2803017 8372333 0
4529348 894645 0
5096572 4211638 0""")

features_data = StringIO("""node_id f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13
6636127 14 14 14 12 12 12 7 7 7 0 0 0 15
8446602 31 9 7 31 16 12 31 15 12 31 15 12 8
4 0 0 0 0 0 0 0 0 0 0 0 0 7
5 31 4 1 31 7 1 31 9 1 31 9 0 15
6 31 27 20 31 24 14 31 20 10 31 20 5 7""")

train_df = pd.read_csv(train_data, sep=" ")
features_df = pd.read_csv(features_data, sep=" ")

print(train_df.merge(features_df, left_on="node1_id", right_on="node_id").merge(features_df, left_on="node2_id", right_on="node_id"))
