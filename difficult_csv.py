import openpyxl
import pandas as pd
import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO

csvdata = StringIO("""MTS793|MPT|ENU|1|2|.|/|:|1|0|0|A

Operator Information						Time:	15.133789	Sec	8/2/2018 12:18:30 PM
Is the extensometer pin pulled?	ughiopuh;u
Operator Information End

Data Acquisition			Tensile Test Data			Time:	117.81934	Sec	8/2/2018 12:20:13 PM
Time	Axial Displacement	Axial Strain	Axial Force
Sec	in	in/in	lbf
15.372559	-0.00026854035	-0.00013428145	-3.7030973
15.472656	2.2532094e-08	-8.3925901e-05	11.109222
15.572754	-0.00026854035	-0.00011749626	3.7030623
15.672852	2.2532094e-08	-0.00011749626	-1.7583034e-05
15.772949	0.00026858543	-0.00010071108	7.4061422""")

df = pd.read_csv(csvdata, sep="\t", header=[5,6])

# Show how pandas parses the file headers
print(df.head(6))

# The columns are referenceable as a multiindex
level0 = df.columns.get_level_values(0)
level1 = df.columns.get_level_values(1)

# Set index from multiindex columns!
# But we are not going to assign this result, because 
# there is a better trick
print(df.set_index(('Time', 'Sec')))

flattened_cols = (["{} ({})".format(x,y) for x,y in zip(level0, level1)])
df.columns = flattened_cols

# The better trick
df.set_index(flattened_cols[0], inplace=True)

# show results
print(df.head(6))

# save in desired file format
df.to_excel('specimen.xlsx')


