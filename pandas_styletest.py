import pandas as pd
import numpy as np

midx = pd.MultiIndex.from_product([['A0','A1'], ['B0','B1','B2','B3']])
columns = ['foo', 'bar']
df = pd.DataFrame(np.arange(16).reshape((len(midx), len(columns))),index=midx, columns=columns)

def style(val):
    print("***", val)
    return 'background-color: yellow'

print(df)

#idx = pd.IndexSlice[:, 'B0':'B1']
#idx = pd.IndexSlice['A0':'A1', 'B0':'B1']
idx = pd.IndexSlice
i = idx[:,'B0':'B1']
print(i)
print(df.loc[i,:])
#print(df.loc[idx[idx[:,'B0':'B1'],:]])

df.style.apply(style, subset=i).to_excel('styletest.xlsx', engine='openpyxl')
