import pandas as pd
from pandas.compat import StringIO
print(pd.__version__)

data = """index   date            hour
0   2018-03-24      23
1   2018-03-24      24
2   2018-03-25      1
3   2018-03-25      2
4   2018-03-25      3
5   2018-03-25      4
6   2018-03-25      5
7   2018-03-25      6
8   2018-03-25      7
9   2018-03-25      8
10  2018-03-25      9
11  2018-03-25      10
12  2018-03-25      11
13  2018-03-25      12
14  2018-03-25      13
15  2018-03-25      14
16  2018-03-25      15
17  2018-03-25      16
18  2018-03-25      17
19  2018-03-25      18
20  2018-03-25      19
21  2018-03-25      20
22  2018-03-25      21
23  2018-03-25      22
24  2018-03-25      23
25  2018-03-26      1
26  2018-03-26      2
27  2018-03-26      3
28  2018-03-26      4
29  2018-03-26      5
30  2018-03-26      6
31  2018-10-27      23
32  2018-10-27      24
33  2018-10-28      1
34  2018-10-28      2
35  2018-10-28      3
36  2018-10-28      4
37  2018-10-28      5
38  2018-10-28      6
39  2018-10-28      7
40  2018-10-28      8
41  2018-10-28      9
42  2018-10-28      10
43  2018-10-28      11
44  2018-10-28      12
45  2018-10-28      13
46  2018-10-28      14
47  2018-10-28      15
48  2018-10-28      16
49  2018-10-28      17
50  2018-10-28      18
51  2018-10-28      19
52  2018-10-28      20
53  2018-10-28      21
54  2018-10-28      22
55  2018-10-28      23
56  2018-10-28      24
57  2018-10-28      25
58  2018-10-29      1
59  2018-10-29      2"""

df = pd.read_csv(StringIO(data), sep='\s+', index_col=0)
df['hour'] = pd.to_timedelta(df['hour'] - 1, 'h')
df['date'] = pd.to_datetime(df['date'])
df['naive_datetime'] = df['date'] + df['hour']
df.set_index(df['naive_datetime'], inplace=True)

# receives non-existent time exception because of naive datetime that does not exist in Europe/London
#df.index = df.index.tz_localize('Europe/London').tz_convert('UTC')

# receives AmbiguousTimeError: Cannot infer dst time from 2018-10-28 01:00:00 as there are no repeated times
#df.index = df.index.tz_localize('Europe/London', ambiguous='infer').tz_convert('UTC')

df.index = df.index.tz_localize('Europe/London', ambiguous='NaT', nonexistent='NaT').tz_convert('UTC')
df.index.name = "datetime Europe/London"

# if there is timestamped data in the dataframe, something has to be done with it.
# The data for the missing time is probably best dropped
#df = df[df.index.notnull()]

# interpolate doesn't work: https://github.com/pandas-dev/pandas/issues/11701, but then again the time does not exist in local tz...
#df['datetime Europe/London interpolated'] = df.index.to_series().interpolate(method='linear')

print(df)
