import pandas as pd
import numpy as np
print(pd.__version__)

# this does not work
df = pd.DataFrame(np.random.choice([np.nan,1,8], size=(10,2)), columns=['a', 'b'])
df[df.shift().eq(8) & df.isnull()] = 8
for col in df.columns:
    filters = df[col].eq(8) | df[col].isnull()
    df.loc[filters,col] = df.loc[filters,col].ffill()

exit(0)

# this does
for col in df.columns:
    new_col_1 = "{}_1".format(col)
    df[new_col_1] = df[col].fillna(8)
    new_col_2 = "{}_2".format(col)
    df[new_col_2] = df[col].ffill()

    df[col] = df[col].ffill()
    df[col][df[new_col_1] != df[new_col_2]] = np.nan
    df.drop([new_col_1, new_col_2], axis=1, inplace=True)

print(df)


