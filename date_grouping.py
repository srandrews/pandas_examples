import pandas as pd
import random as r

daterange = pd.date_range('1952-01-01', '2018-01-01', freq='MS')
df = pd.DataFrame(index=daterange, data={'values': [r.randint(-10,110) for i in range(len(daterange))]})
print(type(df.index))
grouper = df.groupby([df.index.year, df.index.month])
