import pandas as pd
import csv

import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO

csvdata = StringIO("""
Date/time,Syslogpriority,Operation,Messagecode,Protocol,SourceIP,DestinationIP,Sourcehostname,Destinationhostname,Sourceport,Destinationport,Destinationservice,Direction,Connectionsbuilt,Connectionstorndown
2019-08-01 01:00:00,a,b,c,d,e1,f,g,h,i,j,k,l,m,n
2019-08-01 02:00:00,a,b,c,d,e1,f,g,h,i,j,k,l,m,n
2019-08-01 02:30:00,a,b,c,d,e1,f,g,h,i,j,k,l,m,n
2019-08-01 02:40:00,a,b,c,d,e1,f,g,h,i,j,k,l,m,n
2019-08-01 02:00:00,a,b,c,d,e1,f,g,h,i,j,k,l,m,n
2019-08-01 01:00:00,a,b,c,d,e2,f,g,h,i,j,k,l,m,n
2019-08-01 02:00:00,a,b,c,d,e2,f,g,h,i,j,k,l,m,n
2019-08-01 03:00:00,a,b,c,d,e3,f,g,h,i,j,k,l,m,n """)

concat_df = pd.read_csv(csvdata, sep=",", index_col="Date/time", parse_dates=True, infer_datetime_format=True)
concat_df['hour'] = concat_df.index.hour

grouped_df = concat_df.groupby([concat_df['hour'], concat_df['SourceIP']])['SourceIP'].count()

# join back to source data
result_df = concat_df.join(grouped_df, on=['hour', 'SourceIP'], rsuffix='_Occurrence')
print(result_df[['hour', 'SourceIP', 'SourceIP_Occurrence']])

grouped_df = concat_df.groupby([concat_df['hour'], concat_df['SourceIP']=='e1'])['SourceIP'].count()
print(grouped_df)
