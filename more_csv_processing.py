import pandas as pd
import json
from pandas.compat import StringIO
print(pd.__version__)



##### Python pandas, widen output display to see more columns. ####
#pd.set_option('display.height', None)
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('expand_frame_repr', True)
##################### END OF THE Display Settings ###################
patt = "AIX|CentOS|RHEL|SunOS|SuSE|Ubuntu|Fedora|\?"
#col_names = ['Hostname', 'IP Address', 'Aux Site', 'CPU Model', 'CDN Version', 'OS Version', 'Kernel Version', 'LDAP Profile', 'Network Name']

csvdata = StringIO("""Hostname,IP Address,Aux Site,OS Version,Network Name
host01,192.168.1.1,yoko,RHEL,5.5,CISCO
host02,192.168.1.2,chelmsford,AIX,6.1
host03,192.168.1.3,sanjose,RHEL,5.5
host04,192.168.1.4,rosh,CentOS,6.8,CISCO
host05,192.168.1.5,noida3,CentOS,5.10,CISCO
host06,192.168.1.6,rosh,RHEL,6.5,CISCO
host07,192.168.1.7,noida3,RHEL,6.5,CISCO
host08,192.168.1.8,san,jose,RHEL,6.5,CISCO
host10,192.168.1.10,sophia,RHEL,5.5,AVAYA
host11,192.168.1.11,sanjose,RHEL,5.5,AVAYA
host12,192.168.1.12,sanjose,RHEL,5.3,AVAYA
host13,192.168.1.13,sanjose,RHEL,5.8,AVAYA
""")

#csv_df = pd.read_csv(csvdata, sep = "\s+", header = 0, index_col = False)
#df1 = pd.read_csv('/home/karn/plura/Test/Python_Panda/host.txt', delimiter = "\t", usecols=col_names, encoding='cp1252',  dtype='unicode')
# encoding of csvdata is not cp1252 
df1 = pd.read_csv(csvdata, delimiter = ",", encoding='cp1252',  dtype='unicode')

print(df1.columns)
#print(df1['OS Version'])


df2 = df1[df1['OS Version'].str.contains(patt,  na=False)][['Hostname', 'IP Address', 'Aux Site', 'OS Version', 'Network Name']]
df2['Hostname'] = df2['Hostname'].str.replace("*", "")
df2.to_csv("HostList_from_Surveys.csv", sep='\t', encoding='utf-8', index=False)

