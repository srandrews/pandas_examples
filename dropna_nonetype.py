import pandas as pd
import csv
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""Step Age,Process Age,Extra Col
,1,
1,,
,,""")

df = pd.read_csv(csvdata, sep=",")
df.dropna(subset=['Step Age', 'Process Age'], inplace = True)
print(df)
print(df.empty)
