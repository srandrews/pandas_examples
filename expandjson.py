import pandas as pd
from pandas.compat import StringIO
from pandas.io.json import json_normalize
import numpy as np
import json

print(pd.__version__)

pd.set_option('display.width', 1000)
pd.set_option('display.max_columns', None)


d = {"daily": {"apparentTemperatureMaxTime": 1422680400, "temperatureMax": 21.33, "temperatureMinTime": 1422615600, "temperatureMin": 16.06, "icon": "clear-day", "apparentTemperatureMax": 21.33, "summary": "Clear throughout the day.", "pressure": 1010.91, "temperatureMaxTime": 1422680400, "humidity": 0.81, "dewPoint": 15.14, "sunsetTime": 1422692673, "precipType": "rain", "windSpeed": 3.01, "apparentTemperatureMin": 16.06, "sunriseTime": 1422639631, "apparentTemperatureMinTime": 1422615600, "time": 1422615600, "visibility": 16.09, "windBearing": 75, "moonPhase": 0.38}, "hourly": [{"apparentTemperature": 16.06, "windSpeed": 4.29, "icon": "clear-night", "temperature": 16.06, "summary": "Clear", "pressure": 1015.05, "humidity": 0.89, "dewPoint": 14.23, "precipType": "rain", "time": 1422615600, "visibility": 16.09, "windBearing": 97}, {"apparentTemperature": 16.17, "windSpeed": 4.22, "icon": "clear-night", "temperature": 16.17, "summary": "Clear", "pressure": 1014.91, "humidity": 0.88, "dewPoint": 14.19, "precipType": "rain", "time": 1422619200, "visibility": 16.09, "windBearing": 94}, {"apparentTemperature": 16.27, "windSpeed": 4.09, "icon": "clear-night", "temperature": 16.27, "summary": "Clear", "pressure": 1014.51, "humidity": 0.87, "dewPoint": 14.14, "precipType": "rain", "time": 1422622800, "visibility": 16.09, "windBearing": 87}, {"apparentTemperature": 16.36, "windSpeed": 4, "icon": "clear-night", "temperature": 16.36, "summary": "Clear", "pressure": 1013.94, "humidity": 0.86, "dewPoint": 14.09, "precipType": "rain", "time": 1422626400, "visibility": 16.09, "windBearing": 80}, {"apparentTemperature": 16.4, "windSpeed": 3.9, "icon": "clear-night", "temperature": 16.4, "summary": "Clear", "pressure": 1013.43, "humidity": 0.86, "dewPoint": 14.07, "precipType": "rain", "time": 1422630000, "visibility": 16.09, "windBearing": 75}]}



data = [{'lats':-46,'lngs':168,'date':pd.to_datetime('2015-01-31'),'blob':json.dumps(d)}]
df = pd.DataFrame(data)

def myfunc(x):
    blob = x['blob']
    blob = json.loads(blob)
    x['hour'] = [i[0] for i in enumerate(blob['hourly'])]
    x['temp'] = [i[1]['apparentTemperature'] for i in enumerate(blob['hourly'])]
    return(x)

df = df.apply(myfunc, axis=1)
df.drop(['blob'], axis=1, inplace=True)
print(df)
#print(np.repeat(df['hour'].values, len(df['hour'].values)))
print(type(df['hour'].apply(pd.Series)))
print(df['hour'].apply(pd.Series))
