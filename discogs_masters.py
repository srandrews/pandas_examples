import xml.etree.ElementTree as et
import re
import pandas as pd
parser = et.XMLParser()
discogs_masters = """<masters>
<master id="18500"><main_release>155102</main_release><images><image height="588" type="primary" uri="" uri150="" width="600" /></images><artists><artist><id>212070</id><name>Samuel L Session</name><anv>Samuel L</anv><join /><role /><tracks /></artist></artists><genres><genre>Electronic</genre></genres><styles><style>Techno</style></styles><year>2001</year><title>New Soil</title><data_quality>Correct</data_quality><videos><video duration="489" embed="true" src="http://www.youtube.com/watch?v=f05Ai921itM"><title>Samuel L - Velvet</title><description>Samuel L - Velvet</description></video><video duration="292" embed="true" src="http://www.youtube.com/watch?v=iOQsBOJLbwg"><title>Samuel L. - Danshes D'afrique</title><description>Samuel L. - Danshes D'afrique</description></video><video duration="348" embed="true" src="http://www.youtube.com/watch?v=v23rSPG_StA"><title>Samuel L - Danses D'Afrique</title><description>Samuel L - Danses D'Afrique</description></video><video duration="288" embed="true" src="http://www.youtube.com/watch?v=tHo82ha6p40"><title>Samuel L - Body N' Soul</title><description>Samuel L - Body N' Soul</description></video><video duration="331" embed="true" src="http://www.youtube.com/watch?v=KDcqzHca5dk"><title>Samuel L - Into The Groove</title><description>Samuel L - Into The Groove</description></video><video duration="334" embed="true" src="http://www.youtube.com/watch?v=3DIYjJFl8Dk"><title>Samuel L - Soul Syndrome</title><description>Samuel L - Soul Syndrome</description></video><video duration="325" embed="true" src="http://www.youtube.com/watch?v=_o8yZMPqvNg"><title>Samuel L - Lush</title><description>Samuel L - Lush</description></video><video duration="346" embed="true" src="http://www.youtube.com/watch?v=JPwwJSc_-30"><title>Samuel L - Velvet ( Direct Me )</title><description>Samuel L - Velvet ( Direct Me )</description></video></videos></master>
<master id="18512"><main_release>33699</main_release><images><image height="150" type="primary" uri="" uri150="" width="150" /><image height="592" type="secondary" uri="" uri150="" width="600" /><image height="592" type="secondary" uri="" uri150="" width="600" /></images><artists><artist><id>212070</id><name>Samuel L Session</name><anv /><join /><role /><tracks /></artist></artists><genres><genre>Electronic</genre></genres><styles><style>Tribal</style><style>Techno</style></styles><year>2002</year><title>Psyche EP</title><data_quality>Correct</data_quality><videos><video duration="376" embed="true" src="http://www.youtube.com/watch?v=c_AfLqTdncI"><title>Samuel L. Session - Psyche Part 1</title><description>Samuel L. Session - Psyche Part 1</description></video><video duration="419" embed="true" src="http://www.youtube.com/watch?v=0nxvR8Zl9wY"><title>Samuel L. Session - Psyche Part 2</title><description>Samuel L. Session - Psyche Part 2</description></video><video duration="118" embed="true" src="http://www.youtube.com/watch?v=QYf4j0Pd2FU"><title>Samuel L. Session - Arrival</title><description>Samuel L. Session - Arrival</description></video></videos></master>
</masters>
"""
parser.feed(discogs_masters)
root = parser.close()

masters_df_cols = ["MasterID", "MainRelese", "Title", "Year", 
"Genre", "ArtistID", "ArtistName"]
masters_rows = []

for elem in root.iter('master'):
    if elem is not None:
        masterID = str(elem.get('id'))
        mainRelease = str(elem.find('main_release').text)
        year = str(elem.find('year').text)
        title = str(elem.find('title').text)
        genre = str(elem.find('./genres/genre').text)
        artistID = str(elem.find('./artists/artist/id').text)
        artistName = str(elem.find('./artists/artist/name').text)
    masters_rows.append([masterID, mainRelease, year, title, genre, artistID, artistName])

masters_df = pd.DataFrame(masters_rows, columns = masters_df_cols)
print(masters_df)
