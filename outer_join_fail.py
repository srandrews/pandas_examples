import pandas as pd
import csv
from pandas.compat import StringIO

print(pd.__version__)

csvdata = StringIO("""a,b,c,d
111,8,null,0
122,3,null,0
123,9,null,0
254,5,111,1
265,8,111,1
298,7,122,1
220,6,123,1
305,5,298,2
395,8,220,2""")
df1 = pd.read_csv(csvdata, sep=",")
df1_dtypes = df1.dtypes
print(df1_dtypes)

csvdata = StringIO("""a,b,e,f
111,3,,0
122,3,,0
123,9,null,0
254,5,111,1
265,8,111,1
298,7,122,1
220,6,123,1
305,5,298,2
395,8,220,2""")
df2 = pd.read_csv(csvdata, sep=",")
df2_dtypes = df2.dtypes
print(df2_dtypes)

result_df = pd.merge(df1, df2, on=None, how='outer')
print(result_df)

#dtypes_diff = df1_dtypes.merge(df2_dtypes,indicator = True, how='left', on=)
dtypes_diff = pd.concat([df1.dtypes,df2.dtypes]).drop_duplicates(keep=False)
print(dtypes_diff)
