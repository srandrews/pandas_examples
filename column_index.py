import pandas as pd
import csv

import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO

csvdata = StringIO("""
i,Date,B1,B2,B3
0,4022019,7,2,7
1,4012019,3,8,9
2,3312019,5,6,5
3,3302019,9,4,6
4,3292019,4,6,2
5,3282019,1,1,1
6,3272019,0,6,9
7,3262019,9,1,1
8,3252019,9,9,1
9,3242019,3,6,6
""")

df = pd.read_csv(csvdata, sep=",", index_col="Date", parse_dates=True, infer_datetime_format=True)

#df.columns = ['B1','B2']

print(df)

